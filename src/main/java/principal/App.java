package principal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class App {
private static Connection conn = null;
private static String usuario = "PRUEBA";
private static String contrasenia = "12345";
private static String URL = "jdbc:oracle:thin:@localhost:1521:XE:XEPDB1";

public static void main(String[] args) {
	System.out.println("hola mundo");
	Connection con = getConnection();
	String query = "SELECT NOMBRE,EDAD,DEPARTAMENTO,ANTIGUEDAD FROM datos";
	Statement stmt;
	try {
		stmt = con.createStatement();
		ResultSet response = stmt.executeQuery(query);
		while(response.next()) {
			String nombre = response.getString("NOMBRE");
			String edad = response.getString("EDAD");
			String departamento = response.getString("DEPARTAMENTO");
			String antiguedad = response.getString("ANTIGUEDAD");
			System.out.println("RESULTADO Nombre: "+nombre+" edad: "+edad+" departamento: "+departamento+" antiguedad: "+antiguedad);
		}
	} catch(Exception e){
		System.out.println("excepcion...."+e);
		e.printStackTrace();
	}
}
public static Connection getConnection() {
	try {
		Class.forName("oracle.jdbc.OracleDriver");
		conn = DriverManager.getConnection(URL,usuario,contrasenia);
		if(conn!= null) {
			System.out.println("conexion exitosa");
		}
	}catch(Exception e) {
		System.out.println("conexion incorrecta");
		e.printStackTrace();
	}
	return conn;
}
}
